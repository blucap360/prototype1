import React, { useState } from 'react';
import { ViroARScene, ViroConstants, Viro3DObject } from '@viro-community/react-viro';

const ARScreen = (props) => {
  const [arText, setArText] = useState('Initializing AR...');

  const onInitialized = (state, reason) => {
    if (state === ViroConstants.TRACKING_NORMAL) {
      setArText('You are in region');
    } else if (state === ViroConstants.TRACKING_NONE) {
      // Handle loss of tracking
    }
  };

  return (
    <ViroARScene onTrackingUpdated={onInitialized}>
      {props.sceneNavigator.viroAppProps && (
        <Viro3DObject
          source={require('./../../assets/wooden_cube.obj')}
          position={[0.0, 0.0, -1.15]}
          scale={[0.5, 0.5, 0.5]}
          type="OBJ"
        />
      )}
    </ViroARScene>
  );
};

export default ARScreen;
