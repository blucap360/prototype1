import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  button: {
    width: 200,
    height: 50,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: '#000000',
    margin: 10,
  },
  input: {
    height: 40,
    borderWidth: 1,
    marginTop: 15,
    padding: 10,
  },
});

export default styles;
