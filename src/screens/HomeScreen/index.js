import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { ViroARSceneNavigator } from '@viro-community/react-viro';
import GetLocation from 'react-native-get-location';

import ARScreen from '../ARScreen';
import styles from './styles';

const HomeScreen = () => {
  const [isClicked, setIsClicked] = useState(false);
  const [currentCoord, setCurrentCoord] = useState(null);
  const [distance, setDistance] = useState(20);
  const [destLat, setDestLat] = useState(null);
  const [destLon, setDestLon] = useState(null);

  useEffect(() => {
    getLocation();
  }, []);

  const regionFrom = (lat, lon, distance) => {
    distance = distance / 2;
    const circumference = 40075;
    const oneDegreeOfLatitudeInMeters = 111.32 * 1000;
    const angularDistance = distance / circumference;

    const latitudeDelta = distance / oneDegreeOfLatitudeInMeters;
    const longitudeDelta = Math.abs(
      Math.atan2(
        Math.sin(angularDistance) * Math.cos(lat),
        Math.cos(angularDistance) - Math.sin(lat) * Math.sin(lat)
      )
    );

    return {
      latitude: lat,
      longitude: lon,
      latitudeDelta,
      longitudeDelta,
    };
  };

  const getLocation = () => {
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then((location) => {
        const currentRegionObj = regionFrom(location.latitude, location.longitude, distance);
        console.log('->Location', currentRegionObj);
        setCurrentCoord(currentRegionObj);
      })
      .catch((error) => {
        const { code, message } = error;
        console.warn(code, message);
      });
  };

  const onPressBtn = () => {
    const { latitudeDelta, longitudeDelta } = regionFrom(
      currentCoord.latitude,
      currentCoord.longitude,
      distance
    );

    console.log('->Pressed Button', parseFloat(destLat), parseFloat(destLon));

    if (
      currentCoord.latitude < parseFloat(destLat) + latitudeDelta &&
      currentCoord.latitude > parseFloat(destLat) - latitudeDelta &&
      currentCoord.longitude < parseFloat(destLon) + longitudeDelta &&
      currentCoord.longitude > parseFloat(destLon) - longitudeDelta
    ) {
      setIsClicked(true);
      console.log('->You should see box');
    } else {
      setIsClicked(false);
      console.log('->You should not see box');
    }
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <View>
          <Text>{`Current Latitude: ${currentCoord?.latitude}`}</Text>
          <Text>{`Current Longitude: ${currentCoord?.longitude}`}</Text>
          <TextInput
            value={distance}
            onChangeText={(text) => setDistance(text)}
            style={styles.input}
            keyboardType={'numeric'}
            placeholder={'Radius Distance'}
          />
          <TextInput
            value={destLat}
            onChangeText={(text) => setDestLat(text)}
            style={styles.input}
            keyboardType={'numbers-and-punctuation'}
            placeholder={'Destination Latitude'}
          />
          <TextInput
            value={destLon}
            onChangeText={(text) => setDestLon(text)}
            style={styles.input}
            keyboardType={'numbers-and-punctuation'}
            placeholder={'Destination Longitude'}
          />
        </View>
        <TouchableOpacity onPress={onPressBtn} style={styles.button}>
          <Text style={{ color: 'white' }}>{'Press me'}</Text>
        </TouchableOpacity>
        <ViroARSceneNavigator
          autofocus={true}
          initialScene={{
            scene: ARScreen,
          }}
          viroAppProps={isClicked}
          style={{ height: 200 }}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default HomeScreen;
